# General

* error handling
* setup development environment
* check status of parcel
* documentation
* add pretty configuration progress
* allow for sis build update configure
* add sis status
* does zero to synced work?
* build needs to respect selected usisi

## Init features

* interactive but can also be given a config path
  * the config will be overriden by the interactive
  * override a single config with a flag?
* setup the required initial config
  * abosisi root
  * battery
  * adapter
* push binary to gitlab
* clone abosisi from gitlab into the correct location
* make sure neovim is configured initially
* make sure zsh is configured initially
* shell should be changed to zsh by default

## Parcels

* Install dunk github.com/darrenburns/dunk
* Missing packages: Chrome, OBS, Teams, VSCode, IntelliJ community.
  Maybe add Dariel parcel
* Install syncthing, keepassxc, audacity, kdeconnect, trash-cli, dbeaver, tlp, podman
  generate_subgid_and_subuid.py needs to be run for podman
  vim-lsp, lua-lsp, typescript-language-server, purescript-ls
  ranger
