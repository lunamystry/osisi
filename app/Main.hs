module Main where

import Options (
  RunOptions (..),
  makeConfiguration,
  parseOptions,
 )
import Osisi (getDefaultUsisi)
import Osisi.Izenzo.Actions (run)
import Prelude hiding (read)

main :: IO ()
main = do
  defaultUsisi <- getDefaultUsisi
  options <- parseOptions defaultUsisi
  let config = makeConfiguration options
  run config $ runoptionsActions options
