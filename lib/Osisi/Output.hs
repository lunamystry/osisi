module Osisi.Output where

import Control.Exception (SomeException)
import Data.Text (Text)

data Outcome a where
  Exception :: SomeException -> Outcome a -- short circuits
  Error :: Text -> Outcome a -- short circuits
  Warning :: Text -> Outcome a -- can continue
  Success :: Text -> Outcome a

newtype Output a = Output [Outcome a]

fromEither :: Either a b -> Output b
fromEither = undefined
