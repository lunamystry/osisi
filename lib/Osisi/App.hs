module Osisi.App where

import Osisi.Types (SisError, SisSuccess)

-- list can be empty
-- both can be populated

type App = IO [Either SisError SisSuccess]

print :: App -> IO ()
print a = a >>= showAll
  where
    showAll :: [Either SisError SisSuccess] -> IO ()
    showAll = mapM_ showResult
    showResult :: Either SisError SisSuccess -> IO ()
    showResult (Left e) = Prelude.print e
    showResult (Right s) = Prelude.print s

fromError :: SisError -> App
fromError = return . (: []) . Left

fromSuccess :: SisSuccess -> App
fromSuccess = return . (: []) . Right
