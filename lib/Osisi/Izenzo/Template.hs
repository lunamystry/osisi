{-# LANGUAGE DerivingVia #-}

module Osisi.Izenzo.Template (write, renderTemplate, renderTemplateG, writeDhall, mapRendered) where

import Control.Exception (SomeException, try)
import Data.Functor ((<&>))
import Data.Text (Text, pack, toLower)
import Dhall (auto, input)
import Osisi.Amabizo.Path (absFile)
import Osisi.Amabizo.PathTemplate (PathTemplate)
import qualified Osisi.Izenzo.Filesystem as FS
import Osisi.Types (DhallValues, SisError (..), SisSuccess (..), TemplateValues (..), Usisi (..))
import qualified System.Path as Path
import Text.Mustache (compileTemplate)
import Text.Mustache.Render (substitute)

renderTemplate :: TemplateValues -> Text -> IO (Either SisError Text)
renderTemplate vs t = do
  case compileTemplate "generic-template-name" t of
    Left e -> return . Left . StringError $ "could not render template: " <> show e
    Right r -> return . Right . substitute r $ vs

renderTemplateG :: Usisi -> Text -> IO (Either SisError Text)
renderTemplateG u t = do
  values <- getValues u
  renderTemplate values t

getContent :: Path.AbsFile -> IO (Either SisError SisSuccess)
getContent s = do
  res <- try $ Prelude.readFile (Path.toString s)
  case res of
    Left (e :: SomeException) -> return . Left $ ExceptionError e
    Right c -> return . Right $ Success c

writeDhall :: Usisi -> PathTemplate -> PathTemplate -> IO (Either SisError SisSuccess)
writeDhall usisi s d = do
  values <- getValues usisi
  write usisi values s d

getValues :: Usisi -> IO TemplateValues
getValues u = get u <&> GlobalValues
  where
    get :: Usisi -> IO DhallValues
    get = input auto . pack . Path.toString . filePath
    filename = toLower . pack . name
    filePath s = absFile s $ "$DOTS/abosisi/" <> filename u <> ".dhall"

mapRendered :: Usisi -> Text -> (Text -> IO (Either SisError SisSuccess)) -> IO (Either SisError SisSuccess)
mapRendered u c fn = do
  values <- getValues u
  rendered <- renderTemplate values c
  either (return . Left) fn rendered

-- TODO: Simplify this function
write :: Usisi -> TemplateValues -> PathTemplate -> PathTemplate -> IO (Either SisError SisSuccess)
write u values src dst = do
  content <- getContent $ absFile u src
  case content of
    Left e -> return . Left $ e
    Right (Success c) -> do
      rendered <- renderTemplate values (pack c)
      case rendered of
        Left e -> return . Left $ e
        Right r -> FS.write u r dst
    Right s -> return . Right $ s
