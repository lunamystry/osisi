module Osisi.Izenzo.Build (build) where

import Osisi.Amabizo.Commands (command, commandIn)
import Osisi.Izenzo.Commands (run)
import Osisi.Types (SisError, SisSuccess, Usisi (..))

build :: Usisi -> IO [Either SisError SisSuccess]
build u = run u $ commandIn "$HOME/Proyectos/code/osisi" (command "stack --silent build")
