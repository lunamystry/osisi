module Osisi.Izenzo.Commands (run, ifRoot) where

import Control.Monad.Extra (ifM)
import Data.Text (Text, unpack)
import GHC.IO.Exception
import Osisi.Amabizo.Commands (Command (..), Commands)
import Osisi.Amabizo.Path (absDir)
import Osisi.Amabizo.PathTemplate (tmp)
import Osisi.Izenzo.Template (mapRendered)
import Osisi.Types (SisError (..), SisSuccess (..), Usisi)
import System.Directory (createDirectoryIfMissing)
import qualified System.Path as Path
import System.Posix (getEffectiveUserID)
import System.Process (CreateProcess (cwd, delegate_ctlc), createProcess, shell, waitForProcess)

ifRoot :: IO a -> IO a -> IO a
ifRoot = ifM isRoot
  where
    isRoot :: IO Bool
    isRoot = do
      euid <- getEffectiveUserID
      return $ euid == 0

run :: Usisi -> Commands -> IO [Either SisError SisSuccess]
run u = mapM run'
  where
    run' :: Command -> IO (Either SisError SisSuccess)
    run' c = case c of
      Run "" cmd -> callRendered u (absDir u tmp) cmd
      Run dir cmd -> callRendered u (absDir u dir) cmd
      RunAsIs "" cmd -> callAsIs (absDir u tmp) cmd
      RunAsIs dir cmd -> callAsIs (absDir u dir) cmd

callAsIs :: Path.AbsDir -> Text -> IO (Either SisError SisSuccess)
callAsIs dir cmd = putStrLn "" >> callIn dir cmd

callRendered :: Usisi -> Path.AbsDir -> Text -> IO (Either SisError SisSuccess)
callRendered u dir cmd = putStrLn "" >> mapRendered u cmd (callIn dir)

callIn :: Path.AbsDir -> Text -> IO (Either SisError SisSuccess)
callIn dir cmd = mkdirP >> runIt (unpack cmd) >>= waitForIt >>= handleResult
  where
    mkdirP = createDirectoryIfMissing True (Path.toString dir)
    runIt c = createProcess (shell c) {cwd = Just (Path.toString dir), delegate_ctlc = True}
    waitForIt (_, _, _, p) = waitForProcess p -- this can throw a UserInterrupt cause of delegate_ctlc=True
    handleResult result = case result of
      ExitSuccess -> return . Right . Success $ "called '" <> unpack cmd <> "' in " <> Path.toString dir
      ExitFailure r -> return . Left . error $ "call '" <> unpack cmd <> "' failed: " <> show r
