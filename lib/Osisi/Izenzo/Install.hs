module Osisi.Izenzo.Install (install) where

import Data.Text (unwords)
import Osisi.Amabizo.AppConfig (AppConfig (..), selectedParcels)
import Osisi.Amabizo.Commands (Commands, command)
import qualified Osisi.Amabizo.Install as Install
import Osisi.Amabizo.Packages (Packages)
import Osisi.Amabizo.Parcel (Parcel (..))
import qualified Osisi.Amabizo.Parcel as Parcel
import Osisi.Izenzo.Commands (run)
import Osisi.Types (SisError (..), SisSuccess (..), Usisi (..))
import Prelude hiding (unwords, words)

install :: AppConfig -> IO [Either SisError SisSuccess]
install c = do
  sp <- installSystemPackages (usisi c) (selectedParcels c)
  sr <- runInstallCommands (usisi c) (selectedParcels c)
  return $ sp <> sr

runInstallCommands :: Usisi -> [Parcel] -> IO [Either SisError SisSuccess]
runInstallCommands u = runCommands . concatMap parcelCommands
  where
    parcelCommands :: Parcel -> Commands
    parcelCommands = concatMap Install.commands . Parcel.installs
    runCommands :: Commands -> IO [Either SisError SisSuccess]
    runCommands [] = return . (:[]) . Right . Success $ "no commands to run for installation"
    runCommands cs = run u cs

installSystemPackages :: Usisi -> [Parcel] -> IO [Either SisError SisSuccess]
installSystemPackages u = runInstall . concatMap parcelPackages
  where
    parcelPackages :: Parcel -> Packages
    parcelPackages = concatMap Install.packages . Parcel.installs
    runInstall :: Packages -> IO [Either SisError SisSuccess]
    runInstall [] = return . (:[]) . Right . Success $ "no system packages to install"
    runInstall ps = run u . command . ("yay -Sy --noconfirm --needed " <>) . unwords $ ps
