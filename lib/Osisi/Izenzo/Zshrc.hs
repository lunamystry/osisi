{-# LANGUAGE DerivingVia #-}

module Osisi.Izenzo.Zshrc (write) where

import Data.List (intercalate, partition)
import Data.Text (pack)
import Osisi.Amabizo.Zsh (ZshGroup)
import qualified Osisi.Izenzo.Template as Template
import Osisi.Types (Output, SisSuccess (..), TemplateValues (..), Usisi)

write :: Usisi -> [Output] -> IO [Output]
write _ [] = return []
write u output = do
  let (haveGroups, other) = partition hasZshGroups output
  if null haveGroups
    then return other
    else do
      out <- writeZshrc u . values $ concatMap getZshGroups haveGroups
      return $ other <> [out]
  where
    values :: [ZshGroup] -> TemplateValues
    values za = ZshrcValues . pack $ intercalate "\n" (show <$> za)

hasZshGroups :: Output -> Bool
hasZshGroups output =
  case output of
    Right (ZshGroupsAdded _) -> True
    Right _ -> False
    Left _ -> False

getZshGroups :: Output -> [ZshGroup]
getZshGroups output =
  case output of
    Left _ -> []
    Right (ZshGroupsAdded gs) -> gs
    Right _ -> []

writeZshrc :: Usisi -> TemplateValues -> IO Output
writeZshrc usisi values =
  do
    let src = "$DOTS/zsh/zshrc"
        dst = "$HOME/.zshrc"
    Template.write usisi values src dst
