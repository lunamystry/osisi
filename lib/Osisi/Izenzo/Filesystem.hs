module Osisi.Izenzo.Filesystem (
  copyAction,
  mkdirP,
  cp,
  rmdir,
  write,
  writeTextFile,
  readLine,
  touch,
) where

import Control.Exception (
  SomeException,
  throw,
  try,
 )
import Control.Monad (forM_, when)
import Control.Monad.Extra (ifM)
import Data.Text (Text, unpack)
import qualified Data.Text.IO as TIO
import Osisi.Amabizo.Path ((</>))
import qualified Osisi.Amabizo.Path as Path
import Osisi.Amabizo.PathTemplate (PathTemplate)
import Osisi.Types (SisError (..), SisSuccess (..), Usisi (..))
import qualified System.Directory as U
import System.Path.IO (IOMode (ReadMode), hClose, openFile)
import System.Posix.Files (touchFile)
import Prelude hiding (read, readFile)

data CopyAction where
  DirToDir :: Path.AbsDir -> Path.AbsDir -> CopyAction
  FileIntoDir :: Path.AbsFile -> Path.AbsDir -> CopyAction
  FileToFile :: Path.AbsFile -> Path.AbsFile -> CopyAction
  InvalidPath :: String -> CopyAction

copyAction :: Usisi -> PathTemplate -> PathTemplate -> CopyAction
-- src/ dst/ - DirToDir
-- src/ dst - InvalidPath
-- src dst/ - FileIntoDir
-- src dst - FileToFile
copyAction u s d
  | Path.isDir s =
      if Path.isDir d
        then DirToDir (Path.absDir u s) (Path.absDir u d)
        else InvalidPath $ "Invalid combination: " <> unpack s <> " -> " <> unpack d
  | Path.isDir d = FileIntoDir (Path.absFile u s) (Path.absDir u d)
  | otherwise = FileToFile (Path.absFile u s) (Path.absFile u d)

copy :: CopyAction -> IO (Either SisError SisSuccess)
copy (FileToFile s d) = copyFile s d
copy (FileIntoDir s d) = copyFile s (d </> Path.takeFileName s)
copy (DirToDir s d) = copyDir s d
copy (InvalidPath e) = return . Left $ StringError ("Invalid path error: " <> e)

copyFile :: Path.AbsFile -> Path.AbsFile -> IO (Either SisError SisSuccess)
copyFile s d = do
  res <- try $ U.copyFileWithMetadata (Path.toString s) (Path.toString d)
  case res of
    Left (e :: SomeException) -> return . Left $ ExceptionError e
    Right () -> return . Right . Success $ "copied file " <> Path.toString s <> " -> " <> Path.toString d

copyDir :: Path.AbsDir -> Path.AbsDir -> IO (Either SisError SisSuccess)
copyDir s d = do
  res <- try $ copyDirectoryRecursively (Path.toString s) (Path.toString d)
  case res of
    Left (e :: SomeException) -> return . Left $ ExceptionError e
    Right () -> return . Right . Success $ "copied directory " <> Path.toString s <> " -> " <> Path.toString d

copyDirectoryRecursively :: FilePath -> FilePath -> IO ()
copyDirectoryRecursively src dst = do
  whenM (not <$> U.doesDirectoryExist src) $ throw (userError ("source '" <> src <> "' does not exist"))
  whenM (doesFileOrDirectoryExist dst) $ throw (userError ("destination '" <> dst <> "'already exists"))
  U.createDirectory dst
  content <- U.getDirectoryContents src
  let xs = filter (`notElem` [".", ".."]) content
  forM_ xs $ \filename -> do
    let srcPath = src <> "/" <> filename
    let dstPath = dst <> "/" <> filename
    isDirectory <- U.doesDirectoryExist srcPath
    if isDirectory
      then copyDirectoryRecursively srcPath dstPath
      else U.copyFile srcPath dstPath
  where
    doesFileOrDirectoryExist x = orM [U.doesDirectoryExist x, U.doesFileExist x]
    orM xs = or <$> sequence xs
    whenM s r = s >>= flip Control.Monad.when r

cp :: Usisi -> PathTemplate -> PathTemplate -> IO (Either SisError SisSuccess)
cp u s d = copy $ copyAction u s d

-- touch
---------------

data TouchAction where
  TouchDir :: Path.AbsDir -> TouchAction
  TouchFile :: Path.AbsFile -> TouchAction

touch :: Usisi -> PathTemplate -> IO (Either SisError SisSuccess)
touch u d = createOrUpdate $ touchAction u d

createOrUpdate :: TouchAction -> IO (Either SisError SisSuccess)
createOrUpdate (TouchDir d) = do
  _ <- ensureDir d
  return . Right . Success $ "touched dir" <> Path.toString d
createOrUpdate (TouchFile d) = do
  _ <- ensureFile d
  return . Right . Success $ "touched file" <> Path.toString d

touchAction :: Usisi -> PathTemplate -> TouchAction
-- dst/ - TouchDir
-- dst - TouchFile
touchAction u d
  | Path.isDir d = TouchDir (Path.absDir u d)
  | otherwise = TouchFile (Path.absFile u d)

ensureFile :: Path.AbsFile -> IO Path.AbsFile
ensureFile d =
  ifM
    (U.doesFileExist file)
    (return d)
    (touchFile file >> return d)
  where
    file = Path.toString d

-- remove
---------------

removePath :: Path.AbsDir -> IO (Either SisError SisSuccess)
removePath s = do
  res <- try $ U.removePathForcibly (Path.toString s)
  case res of
    Left (e :: SomeException) -> return . Left $ ExceptionError e
    Right () -> return . Right . Success $ "removed path " <> Path.toString s

rmdir :: Usisi -> PathTemplate -> IO (Either SisError SisSuccess)
rmdir u d = removePath $ Path.absDir u d

-- mkdir
----------

ensureDir :: Path.AbsDir -> IO Path.AbsDir
ensureDir d =
  ifM
    (U.doesDirectoryExist dir)
    (return d)
    (U.createDirectoryIfMissing True dir >> return d)
  where
    dir = Path.toString d

mkdirP :: Usisi -> PathTemplate -> IO Path.AbsDir
mkdirP u d = ensureDir $ Path.absDir u d

-- write
----------

writeTextFile :: Text -> Path.AbsFile -> IO (Either SisError SisSuccess)
writeTextFile content dst = do
  _ <- ensureDir (Path.takeDirectory dst)
  res <- try $ TIO.writeFile (Path.toString dst) content
  case res of
    Left (e :: SomeException) -> return . Left . ExceptionError $ e
    Right () -> return . Right . Success $ "wrote file " <> Path.toString dst

write :: Usisi -> Text -> PathTemplate -> IO (Either SisError SisSuccess)
write u c d = writeTextFile c $ Path.absFile u d

-- read
----------

readTextLine :: Path.AbsFile -> IO Text
readTextLine tokenFile = do
  handle <- openFile tokenFile ReadMode
  content <- TIO.hGetLine handle
  hClose handle
  return content

readLine :: Usisi -> PathTemplate -> IO Text
readLine u f = readTextLine (Path.absFile u f)
