module Osisi.Amabizo.Install where

import Osisi.Amabizo.Commands (Commands)
import Osisi.Amabizo.Packages (Packages)

data Install where
  NoInstall :: Install
  Install :: Packages -> Commands -> Install

commands :: Install -> Commands
commands NoInstall = []
commands (Install _ c) = c

packages :: Install -> Packages
packages NoInstall = []
packages (Install p _) = p
