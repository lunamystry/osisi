module Osisi.Amabizo.Packages where

import Data.Text (Text)

type Package = Text

type Packages = [Package]
