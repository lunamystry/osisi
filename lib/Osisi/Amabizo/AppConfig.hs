module Osisi.Amabizo.AppConfig where

import Data.Text (Text, pack, words)
import Osisi.Amabizo.Parcel (Parcel (..))
import Osisi.Types (Usisi (..))
import Prelude hiding (words)

data Selected where
  All :: Selected
  Selected :: [Text] -> Selected
  deriving (Show)

data AppConfig where
  AppConfig ::
    { trace :: [Text]
    , selected :: Selected
    , usisi :: Usisi
    } ->
    AppConfig
  deriving (Show)

selectedParcels :: AppConfig -> [Parcel]
selectedParcels c = filter (isSelected $ selected c) . parcels . usisi $ c
  where
    isSelected :: Selected -> Parcel -> Bool
    isSelected All _ = True
    isSelected (Selected ps) (Parcel n _) = n `elem` ps

selectedFromString :: String -> Either String Selected
selectedFromString "all" = return All
selectedFromString a = return . Selected . words . pack $ a
