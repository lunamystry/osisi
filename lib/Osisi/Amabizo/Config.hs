module Osisi.Amabizo.Config where

import Data.Text (Text)
import Osisi.Amabizo.Commands (Commands)
import Osisi.Amabizo.PathTemplate (PathTemplate, (</>))
import Osisi.Amabizo.Zsh (ZshGroup (..))

type SourcePath = PathTemplate

type DestinationPath = PathTemplate

type Content = Text

data Item where
  RemoveItems :: PathTemplate -> Item
  TouchItems :: PathTemplate -> Item
  ResetItems :: PathTemplate -> Item
  CopyItems :: SourcePath -> DestinationPath -> Item
  WriteItems :: Content -> DestinationPath -> Item
  TemplateItems :: SourcePath -> DestinationPath -> Item
  ZshGroups :: [ZshGroup] -> Item
  CommandItems :: Commands -> Item

data Config where
  Config :: [Item] -> Config
  EmptyConfig :: Config

emptyConfig :: Config
emptyConfig = Config []

remove :: PathTemplate -> [Item]
remove d = [RemoveItems d]

touch :: PathTemplate -> [Item]
touch d = [TouchItems d]

reset :: PathTemplate -> [Item]
reset d = [RemoveItems d]

cmd :: Commands -> [Item]
cmd c = [CommandItems c]

copy :: Text -> SourcePath -> DestinationPath -> [Item]
copy n s d = [CopyItems (s </> n) (d </> n)]

copyTo :: SourcePath -> DestinationPath -> [Item]
copyTo s d = [CopyItems s d]

write :: Content -> DestinationPath -> [Item]
write c d = [WriteItems c d]

template :: Text -> SourcePath -> DestinationPath -> [Item]
template n s d = [TemplateItems (s </> n) (d </> n)]

templateTo :: SourcePath -> DestinationPath -> [Item]
templateTo s d = [TemplateItems s d]

zshGroup :: Text -> [Text] -> [Item]
zshGroup t xs = [ZshGroups [ZshGroup t xs]]
