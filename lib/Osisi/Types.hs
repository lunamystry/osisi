{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE InstanceSigs #-}

module Osisi.Types where

import Control.Exception (SomeException)
import Data.Text (Text)
import Dhall (FromDhall, Generic, ToDhall)
import Dhall.Deriving (Codec (Codec), Field, SnakeCase)
import Osisi.Amabizo.Parcel (Parcel (..))
import Osisi.Amabizo.Zsh (ZshGroup)
import Text.Mustache (ToMustache (..), object, (~>))

data Usisi where
  Usisi ::
    { name :: String
    , home :: Text
    , projectPath :: Text
    , parcels :: [Parcel]
    } ->
    Usisi

instance Show Usisi where
  show :: Usisi -> String
  show = name

data DhallValues where
  DhallValues :: {email :: Text, colourMode :: Text, username :: Text} -> DhallValues
  deriving stock (Generic, Show)
  deriving
    (FromDhall, ToDhall)
    via Codec (Field SnakeCase) DhallValues

data TemplateValues where
  GlobalValues :: DhallValues -> TemplateValues
  ZshrcValues :: {zshrc :: Text} -> TemplateValues
  deriving stock (Generic, Show)

instance ToMustache TemplateValues where
  toMustache (ZshrcValues vs) = object ["zshrc" ~> vs]
  toMustache (GlobalValues (DhallValues e cm u)) = object ["email" ~> e, "colour_mode" ~> cm, "username" ~> u]

data SisError where
  StringError :: String -> SisError
  ExceptionError :: SomeException -> SisError

instance Show SisError where
  show (StringError s) = "Errored: " <> s
  show (ExceptionError e) = "Exception: " <> show e

data SisSuccess where
  Success :: String -> SisSuccess
  ZshGroupsAdded :: [ZshGroup] -> SisSuccess

instance Show SisSuccess where
  show (Success "") = "Success"
  show (ZshGroupsAdded gs) = "Zsh: zsh groups for some reason " <> show gs
  show (Success s) = "Success: " <> s

type PackageName = Text

type Output = Either SisError SisSuccess
