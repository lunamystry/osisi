module Parcels.Starship where

import Osisi.Amabizo.Config (Config (..), copy)
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))

config :: Config
config = Config $ copy "starship.toml" "$DOTS/starship/" "$XDG_CONFIG/"

install :: Install
install = Install packages []
  where
    packages = ["starship"]

parcel :: Parcel
parcel =
  Parcel
    "starship"
    [ ParcelConfig config
    , ParcelInstall install
    ]
