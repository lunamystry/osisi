module Parcels.Scala where

import Osisi.Amabizo.Commands (Commands, command)
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))

update :: Commands
update =
  command "cs update"

install :: Install
install = Install [] commands
  where
    commands =
      command "curl -fL \"https://github.com/coursier/launchers/raw/master/cs-x86_64-pc-linux.gz\" | gzip -d > cs"
        <> command "chmod +x cs"
        <> command "./cs  setup --jvm graalvm-java21 --apps ammonite,bloop,cs,giter8,sbt,scala,scala3-repl,scalafmt"

parcel :: Parcel
parcel =
  Parcel
    "scala"
    [ ParcelUpdate update
    , ParcelInstall install
    ]
