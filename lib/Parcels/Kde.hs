module Parcels.Kde where

import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))

install :: Install
install = Install packages []
  where
    packages =
      [ "dolphin"
      , "dolphin-plugins"
      , "ffmpegthumbs" -- Video previous
      , "taglibs" -- Audio file preview
      , "resvg" -- SVG file previous
      , "amarok" -- Music player
      ]

parcel :: Parcel
parcel =
  Parcel
    "kde"
    [ ParcelInstall install
    ]
