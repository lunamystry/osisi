module Parcels.Printer where

import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))

install :: Install
install = Install packages []
  where
    packages = ["cups", "cups-pdf", "cups-filter", "sane", "xsane"]

parcel :: Parcel
parcel = Parcel "printer" [ParcelInstall install]
