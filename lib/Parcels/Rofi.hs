module Parcels.Rofi where

import Osisi.Amabizo.Config
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))

config :: Config
config =
  Config $
    template "config.rasi" "$DOTS/rofi/" "$XDG_CONFIG/rofi/"
      <> copy "themes/" "$DOTS/rofi/" "$XDG_CONFIG/rofi/"
      <> copy "power-profiles.rasi" "$DOTS/rofi/" "$XDG_CONFIG/rofi/"
      <> copy "powermenu.rasi" "$DOTS/rofi/" "$XDG_CONFIG/rofi/"
      <> copy "rofidmenu.rasi" "$DOTS/rofi/" "$XDG_CONFIG/rofi/"
      <> copy "rofikeyhint.rasi" "$DOTS/rofi/" "$XDG_CONFIG/rofi/"
      <> copy "arc_dark_colors.rasi" "$DOTS/rofi/" "$XDG_CONFIG/rofi/"
      <> copy "arc_dark_transparent_colors.rasi" "$DOTS/rofi/" "$XDG_CONFIG/rofi/"

install :: Install
install = Install packages []
  where
    packages = ["rofi", "papirus-icon-theme"]

parcel :: Parcel
parcel =
  Parcel "rofi" [ParcelConfig config, ParcelInstall install]
