module Parcels.Firefox where

import Osisi.Amabizo.Config (Config (Config), copy)
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))

config :: Config
config = Config $ copy "tridactylrc" "$DOTS/firefox/" "$XDG_CONFIG/tridactyl/"

install :: Install
install = Install packages []
  where
    packages = ["firefox"]

parcel :: Parcel
parcel =
  Parcel "firefox" [ParcelConfig config, ParcelInstall install]
