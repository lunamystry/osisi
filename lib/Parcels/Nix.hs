module Parcels.Nix where

import Osisi.Amabizo.Commands (command)
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))

install :: Install
install = Install packages commands
  where
    packages = ["nix-zsh-completions"]
    commands =
      command "curl --proto '=https' --tlsv1.2 -sSf -L \"https://install.lix.systems/lix -o lix-install.sh\""
        <> command "chmod +x lix-install.sh"
        <> command "./lix-install.sh install"
        <> command "nix-channel --add https://nixos.org/channels/nixpkgs-unstable"
        <> command "nix-channel --update"

parcel :: Parcel
parcel =
  Parcel "nix" [ParcelInstall install]
