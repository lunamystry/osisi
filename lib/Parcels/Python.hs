module Parcels.Python where

import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))

install :: Install
install = Install packages []
  where
    packages = ["python-poetry", "pyright", "uv"]

parcel :: Parcel
parcel =
  Parcel "python" [ParcelInstall install]
