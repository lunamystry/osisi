module Parcels.Zsh where

import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))

install :: Install
install = Install packages []
  where
    packages =
      [ "zsh"
      , "fzf"
      , "zoxide" -- nice cd
      , "direnv" -- auto set environment vars on cd
      , "gibo" -- git things
      , "nvm"
      , "exa" -- nice file listing
      , "ripgrep-all" -- ripgrep that works on other things like pdfs
      , "httpie"
      , "dust"
      , "syncthingtray"
      , "starship" -- nice prompt
      , "exercism"
      , "htop-vim"
      , "fd"
      , "trash-cli" -- safer rm
      -- >>> for control of RGB lights
      , "openrgb"
      , "i2c-tools"
      -- <<<
      ]

parcel :: Parcel
parcel = Parcel "zsh" [ParcelInstall install]
