module Parcels.Haskell where

import Osisi.Amabizo.Commands (Commands, command)
import Osisi.Amabizo.Config (Config (..), copyTo)
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))

config :: Config
config = Config $ copyTo "$DOTS/haskell/ghci" "$HOME/.ghci"

update :: Commands
update =
  command "~/.ghcup/bin/cabal update"
    <> command "~/.ghcup/bin/stack update"
    <> command "ghcup upgrade"

install :: Install
install = Install packages commands
  where
    packages = ["ghcup-hs-bin", "haskell-fourmolu", "hoogle", "haskell-fast-tags"]
    commands =
      command "ghcup install cabal"
        <> command "ghcup install stack"
        <> command "ghcup install hls"

parcel :: Parcel
parcel =
  Parcel
    "haskell"
    [ ParcelConfig config
    , ParcelUpdate update
    , ParcelInstall install
    ]
