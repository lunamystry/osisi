module Parcels.Syncthing where

import Osisi.Amabizo.Commands (command)
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))

install :: Install
install = Install packages commands
  where
    packages = ["syncthing", "syncthingtray"]
    commands = command "systemctl --user enable syncthing"

parcel :: Parcel
parcel =
  Parcel
    "syncthing"
    [ ParcelInstall install
    ]
