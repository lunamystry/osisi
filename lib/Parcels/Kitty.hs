module Parcels.Kitty where

import Osisi.Amabizo.Config (Config (..), copy, template, zshGroup)
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))
import Osisi.Amabizo.Zsh (alias)

config :: Config
config =
  Config $
    copy "current-theme.conf" "$DOTS/kitty/" "$XDG_CONFIG/kitty/"
      <> template "kitty.conf" "$DOTS/kitty/" "$XDG_CONFIG/kitty/"
      <> zshGroup "kitty" (alias "icat" "kitten icat")

install :: Install
install = Install packages []
  where
    packages = ["kitty"]

parcel :: Parcel
parcel =
  Parcel
    "kitty"
    [ ParcelConfig config
    , ParcelInstall install
    ]
