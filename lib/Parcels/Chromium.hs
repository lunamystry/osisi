module Parcels.Chromium where

import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))

install :: Install
install = Install packages []
  where
    packages = ["chromium"]

parcel :: Parcel
parcel =
  Parcel "chromium" [ParcelInstall install]
