module Parcels.Waybar where

import Osisi.Amabizo.Config (Config (Config), template)
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))
import Osisi.Amabizo.PathTemplate (PathTemplate)

dotsDir :: PathTemplate
dotsDir = "$DOTS/waybar/"

configDir :: PathTemplate
configDir = "$XDG_CONFIG/waybar/"

install :: Install
install = Install packages []
  where
    packages = ["waybar"]

config :: Config
config = Config $ template "config" dotsDir configDir

parcel :: Parcel
parcel = Parcel "waybar" [ParcelConfig config, ParcelInstall install]
