module Parcels.Xonsh where

import Osisi.Amabizo.Commands (Commands, command)
import Osisi.Amabizo.Config (Config (..), copy)
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))

config :: Config
config = Config $ copy "rc.xsh" "$DOTS/xonsh/" "$XDG_CONFIG/xonsh/"

installCmds :: Commands
installCmds =
  -- TODO: Create a new kittyCommand for this
  command "kitty xonsh -c \"xpip install 'xonsh[full]'\""
    <> command "kitty xonsh -c \"xpip install xontrib-prompt-starship xontrib-kitty xontrib-zoxide xonsh-direnv xontrib-gitinfo\""

update :: Commands
update = installCmds

install :: Install
install = Install packages installCmds
  where
    packages = ["kitty", "xonsh", "python-prompt_toolkit", "starship", "onefetch"]

parcel :: Parcel
parcel =
  Parcel
    "xonsh"
    [ ParcelConfig config
    , ParcelUpdate update
    , ParcelInstall install
    ]
