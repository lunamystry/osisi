module Parcels.Xmonad where

import Osisi.Amabizo.Commands (Commands, command, commandIn)
import Osisi.Amabizo.Config (Config (Config), cmd, copy)
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))
import Osisi.Amabizo.PathTemplate (PathTemplate, (</>))

dotsDir :: PathTemplate
dotsDir = "$DOTS/xmonad/"

configDir :: PathTemplate
configDir = "$XDG_CONFIG/xmonad/"

restart :: Commands
restart = command "xmonad --recompile && xmonad --restart"

installXmonad :: Commands
installXmonad =
  commandIn configDir $
    command "rm -r xmonad xmonad-contrib"
      <> command "git clone https://github.com/xmonad/xmonad.git"
      <> command "git clone https://github.com/xmonad/xmonad-contrib.git"
      <> command "stack init &>/dev/null"
      <> command "stack install"
      <> restart

config :: Config
config =
  Config $
    copy "lib/" dotsDir configDir
      <> copy "xmonad.hs" dotsDir configDir
      <> copy "package.yaml" dotsDir configDir
      <> copy "keys.sh" dotsDir configDir
      <> copy "autostart.sh" dotsDir configDir
      <> copy "system-overview.conky" dotsDir configDir
      <> copy "picom-toggle.sh" dotsDir configDir
      <> copy "picom.conf" dotsDir configDir
      <> cmd restart

update :: Commands
update =
  commandIn (configDir </> "xmonad") (command "git pull")
    <> commandIn (configDir </> "xmonad-contrib") (command "git pull")
    <> commandIn configDir (command "stack build")
    <> commandIn configDir (command "stack install")
    <> restart

install :: Install
install = Install packages commands
  where
    packages =
      [ "autorandr"
      , "bat"
      , "conky-lua-archers"
      , "dolphin"
      , "dunst"
      , "easyeffects"
      , "eww-git"
      , "fd"
      , "feh"
      , "fzf"
      , "git"
      , "glava" -- a general-purpose, highly configurable OpenGL audio spectrum visualizer for X11
      , "haskell-utf8-string"
      , "haskell-x11"
      , "kdeconnect"
      , "lazygit"
      , "libnotify"
      , "lxappearance"
      , "npm"
      , "picom"
      , "picom-pijulius-git"
      , "playerctl"
      , "polybar"
      , "python-pip"
      , "ttf-fira-code"
      , "ttf-font-awesome"
      , "ttf-jetbrains-mono-nerd"
      , "unclutter"
      , "wmctrl"
      , "xcape" -- use a modifier key as another key
      , "xdo" -- Perform actions on windows
      , "xorg-xinit"
      , "xorg-xmessage"
      , "xorg-xmodmap"
      , "xorg-xsetroot"
      , "zoxide"
      , "rts_bpp-dkms-git" -- A kernel module for realtek card reader
      , "arcolinux-logout" -- a pretty logout script thingy
      -- consider if I need the following
      , "cava" -- Cross-platform Audio Visualizer
      , "beautyline" -- Garuda Linux BeautyLine icons
      , "jgmenu"
      ]
    commands = installXmonad

parcel :: Parcel
parcel =
  Parcel
    "xmonad"
    [ ParcelConfig config
    , ParcelUpdate update
    , ParcelInstall install
    ]
