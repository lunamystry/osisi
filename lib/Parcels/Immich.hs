module Parcels.Immich where

import Osisi.Amabizo.Commands (Commands, command, commandIn)
import Osisi.Amabizo.Config (Config (..), copy, copyTo, reset)
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))
import Osisi.Amabizo.PathTemplate (PathTemplate, (</>))

-- https://immich.app/docs/install/docker-compose/
-- Using podman and the docker compose file doesn't work by default
-- wget -O podman-compose.yml https://github.com/immich-app/immich/releases/latest/download/docker-compose.yml

dotsDir :: PathTemplate
dotsDir = "$DOTS/immich/"

optDir :: PathTemplate
optDir = "$HOME/.local/opt/"

installDir :: PathTemplate
installDir = optDir </> "immich/"

configDir :: PathTemplate
configDir = installDir </> "config/"

librariesDir :: PathTemplate
librariesDir = installDir </> "libraries/"

photosDir :: PathTemplate
photosDir = installDir </> "photos/"

postgresDir :: PathTemplate
postgresDir = installDir </> "postgres/"

enablePorts :: Commands
enablePorts =
  command "sudo firewall-cmd --add-port=5000/tcp --permanent"

update :: Commands
update =
  commandIn installDir (command "podman-compose pull")
    <> commandIn installDir (command "podman-compose up")

install :: Install
install = Install [] commands
  where
    commands = command ""

config :: Config
config =
  Config $
    reset installDir
      <> copy "podman-compose.yml" dotsDir installDir
      <> copyTo (dotsDir </> "env") (installDir </> ".env")
      <> copyTo (dotsDir </> "immich.service") "$XDG_CONFIG/systemd/user/"

parcel :: Parcel
parcel =
  Parcel
    "immich"
    [ ParcelConfig config
    , ParcelUpdate update
    , ParcelInstall install
    ]
