# Java

## Jabba

Using jabba as the version manager. 

Instead running:

    export JABBA_VERSION=latest
    curl -sL https://github.com/shyiko/jabba/raw/master/install.sh | bash -s -- --skip-rc && . ~/.jabba/jabba.sh

I just saved the script as jabba_install.sh

    curl -sL https://github.com/shyiko/jabba/raw/master/install.sh > jabba_install.sh

verified that I am happy with what it is doing and that is the one I use to install.

