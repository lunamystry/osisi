module Wami.Constants where

import XMonad (
  Dimension,
  KeyMask,
  mod4Mask,
 )

myFont :: String
myFont = "xft:Iosevka:regular:size=9:antialias=true:hinting=true"

myModMask :: KeyMask
myModMask = mod4Mask -- Sets modkey to super/windows key

myTerminal :: String
myTerminal = "kitty --single-instance"

myBrowser :: String
myBrowser = "firefox "

myFileBrowser :: String
myFileBrowser = "thunar "

myEditor :: String
myEditor = myTerminal ++ " -e nvim "


myBorderWidth :: Dimension
myBorderWidth = 2

myNormColor :: String
myNormColor = "#282c34"

myFocusColor :: String
myFocusColor = "#46d9ff"
