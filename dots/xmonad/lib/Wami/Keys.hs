module Wami.Keys (workspaceKeys, myKeys) where

import Data.Map qualified as M
import Data.Maybe (isJust)
import System.Exit (exitSuccess)
import Custom.MyDecorations
import Wami.Constants
import XMonad
import XMonad.Actions.CopyWindow (kill1)
import XMonad.Actions.CycleWS (WSType (..), moveTo, nextScreen, prevScreen, shiftTo)
import XMonad.Actions.Promote (promote)
import XMonad.Actions.RotSlaves (rotAllDown, rotSlavesDown)
import XMonad.Actions.WindowGo (runOrRaise)
import XMonad.Actions.WithAll (killAll, sinkAll)
import XMonad.Actions.Search qualified as S
import XMonad.Actions.Submap qualified as SM
import XMonad.Hooks.ManageDocks
import XMonad.Layout.LimitWindows (decreaseLimit, increaseLimit)
import XMonad.Layout.MultiToggle qualified as MT (Toggle (..))
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.ResizableThreeColumns (MirrorResize (..))
import XMonad.Layout.Spacing (decScreenSpacing, decWindowSpacing, incScreenSpacing, incWindowSpacing)
import XMonad.Layout.ToggleLayouts qualified as T (ToggleLayout (Toggle))
import XMonad.StackSet qualified as W

-- START_KEYS
myKeys :: [(String, X ())]
myKeys =
  -- KB_GROUP Xmonad
  [ ("C-S-<Backspace>", io exitSuccess) -- Quits xmonad
  , ("C-S-r", spawn "xmonad --recompile && xmonad --restart")
  , ("C-S-q", spawn "archlinux-logout")
  , ("<F12>", spawn "~/.config/xmonad/keys.sh")
  -- KB_GROUP Kill windows
  , ("M-q", kill1) -- Kill the currently focused client
  , ("M-S-a", killAll) -- Kill all windows on current workspace
  , ("M-<Escape>", spawn "xkill")
  -- KB_GROUP Applications
  , ("M-<Return>", spawn myTerminal)
  , ("M-b", spawn myBrowser)
  , ("M-d", spawn myFileBrowser)
  , ("M-e", spawn "neovide --maximised")
  , ("M-a", spawn $ myTerminal ++ " -e htop")
  , ("M-c", spawn "conky-toggle")
  , ("M-v", spawn "pavucontrol")
  , ("M-p", spawn "polybar-msg cmd toggle")
  -- KB_GROUP Run Prompt
  , ("M-<Space>", spawn "rofi -show drun")
    -- Search commands (wait for next keypress)
  , ("M-s", SM.submap $ searchEngineMap $ S.promptSearchBrowser myPromptConfig myBrowser)
  -- KB_GROUP Variety wallpapers
  , ("M-f", spawn "variety --favorite")
  , ("M-t", spawn "variety --trash")
  , ("M-<Right>", spawn "variety --next")
  , ("M-<Left>", spawn "variety --previous")
  , ("M-<Up>", spawn "variety --pause")
  , ("M-<Down>", spawn "variety --resume")
  -- KB_GROUP Screenshots
  , ("<Print>", spawn "flameshot gui")
  , ("S-<Print>", spawn "flameshot full")
  -- KB_GROUP Workspaces
  , ("M-.", nextScreen) -- Switch focus to next monitor
  , ("M-,", prevScreen) -- Switch focus to prev monitor
  , ("M-S-<KP_Add>", shiftTo Next nonNSP >> moveTo Next nonNSP) -- Shifts focused window to next ws
  , ("M-S-<KP_Subtract>", shiftTo Prev nonNSP >> moveTo Prev nonNSP) -- Shifts focused window to prev ws
  -- KB_GROUP Floating windows
  , ("M-t", withFocused $ windows . W.sink) -- Push floating window back to tile
  , ("M-S-t", sinkAll) -- Push ALL floating windows to tile
  -- KB_GROUP Increase/decrease spacing (gaps)
  , ("C-M1-j", decWindowSpacing 4) -- Decrease window spacing
  , ("C-M1-k", incWindowSpacing 4) -- Increase window spacing
  , ("C-M1-h", decScreenSpacing 4) -- Decrease screen spacing
  , ("C-M1-l", incScreenSpacing 4) -- Increase screen spacing
  -- KB_GROUP Layout management
  , ("C-<Tab>", sendMessage NextLayout) -- Switch to next layout
  , ("M-f", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts) -- Toggles noborder/full
  , ("M-w", sendMessage (T.Toggle "monocle"))
  -- KB_GROUP Windows navigation
  , ("M-m", windows W.focusMaster) -- Move focus to the master window
  , ("M-j", windows W.focusDown) -- Move focus to the next window
  , ("M-k", windows W.focusUp) -- Move focus to the prev window
  , ("M-S-m", windows W.swapMaster) -- Swap the focused window and the master window
  , ("M-S-j", windows W.swapDown) -- Swap focused window with next window
  , ("M-S-k", windows W.swapUp) -- Swap focused window with prev window
  , ("M-<Backspace>", promote) -- Moves focused window to master, others maintain order
  , ("M-S-<Tab>", rotSlavesDown) -- Rotate all windows except master and keep focus in place
  , ("M-C-<Tab>", rotAllDown) -- Rotate all the windows in the current stack
  -- KB_GROUP Increase/decrease windows in the master pane or the stack
  , ("M-S-<Up>", sendMessage (IncMasterN 1)) -- Increase # of clients master pane
  , ("M-S-<Down>", sendMessage (IncMasterN (-1))) -- Decrease # of clients master pane
  , ("M-C-<Up>", increaseLimit) -- Increase # of windows
  , ("M-C-<Down>", decreaseLimit) -- Decrease # of windows
  -- KB_GROUP Window resizing
  , ("M-h", sendMessage Shrink) -- Shrink horiz window width
  , ("M-l", sendMessage Expand) -- Expand horiz window width
  , ("M-M1-j", sendMessage MirrorShrink) -- Shrink vert window width
  , ("M-M1-k", sendMessage MirrorExpand) -- Expand vert window width
  -- KB_GROUP Multimedia Keys
  , ("<XF86AudioMute>", spawn "amixer set Master toggle")
  , ("<XF86AudioLowerVolume>", spawn "amixer set Master 5%- unmute")
  , ("<XF86AudioRaiseVolume>", spawn "amixer set Master 5%+ unmute")
  , ("<XF86HomePage>", spawn "firefox")
  , ("<XF86Search>", spawn "dm-websearch")
  , ("<XF86Mail>", runOrRaise "thunderbird" (resource =? "thunderbird"))
  , ("<XF86Calculator>", runOrRaise "qalculate-gtk" (resource =? "qalculate-gtk"))
  , ("<XF86Eject>", spawn "toggleeject")
  
  ]
  where
    -- The following lines are needed for named scratchpads.
    nonNSP = WSIs (return (\ws -> W.tag ws /= "NSP"))
    nonEmptyNonNSP = WSIs (return (\ws -> isJust (W.stack ws) && W.tag ws /= "NSP"))
-- END_KEYS

workspaceKeys :: XConfig Layout -> M.Map (KeyMask, KeySym) (X ())
workspaceKeys conf@(XConfig {XMonad.modMask = modMask}) =
  M.fromList $
      [ ((modMask .|. shiftMask, xK_space), setLayout $ XMonad.layoutHook conf) -- Reset layouts
      ] ++
      -- mod-[1..9], Switch to workspace N
      -- mod-shift-[1..9], Move client to workspace N
      [ ((m .|. modMask, k), windows $ f i)
      | (i, k) <- zip (XMonad.workspaces conf) [xK_1, xK_2, xK_3, xK_4, xK_5, xK_6, xK_7, xK_8, xK_9, xK_0]
      , (f, m) <-
          [ (W.greedyView, 0)
          , (W.shift, shiftMask)
          , (\j -> W.greedyView j . W.shift j, shiftMask)
          ]
      ]

searchEngineMap method =
  M.fromList
    [ ((0, xK_a), method $ S.searchEngine "archwiki" "http://wiki.archlinux.org/index.php/Special:Search?search="),
      ((0, xK_g), method S.google),
      ((0, xK_h), method S.hoogle),
      ((0, xK_i), method S.imdb),
      ((0, xK_p), method S.aur),
      ((0, xK_s), method $ S.searchEngine "stackoverflow" "https://stackoverflow.com/search?q="),
      ((0, xK_w), method S.wikipedia),
      ((0, xK_y), method S.youtube)
    ]
