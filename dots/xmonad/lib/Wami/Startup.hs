module Wami.Startup (myStartupHook) where

import Wami.Constants
import XMonad
import XMonad.Actions.GridSelect (GSConfig (..), gridselect)
import XMonad.Hooks.SetWMName (setWMName)
import XMonad.Util.SpawnOnce
import XMonad.StackSet qualified as W

myStartupHook :: X ()
myStartupHook = do
  let picomCmd = "killall -9 picom; sleep 2 && picom -b &"
      easyeffectsCmd = "easyeffects --gapplication-service &"
      ewwCmd = "~/.config/eww/scripts/startup.sh"
      autostartScript = "$HOME/.config/xmonad/autostart.sh"
  sequence_ [spawn picomCmd, spawnOnce easyeffectsCmd, spawn ewwCmd, spawn autostartScript]
  setWMName "LG3D"
