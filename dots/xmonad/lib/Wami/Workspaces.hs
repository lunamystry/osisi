module Wami.Workspaces (myWorkspaces, myManageHook) where

import Data.Map qualified as M
import Data.Maybe (fromJust)
import Data.Monoid qualified
import XMonad
import XMonad.Hooks.ManageHelpers

myWorkspaces :: [String]
myWorkspaces = ["1 dev", "2 web", "3 edit", "4 term", "5 doc", "6 img", "7 vid", "8 mus", "9 prod", "10 sys"]

myWorkspaceIndices :: M.Map String Integer
myWorkspaceIndices = M.fromList $ zip myWorkspaces [1 ..] -- (,) == \x y -> (x,y)

clickable :: String -> String
clickable ws = "<action=xdotool key super+" ++ show i ++ ">" ++ ws ++ "</action>"
  where
    i = fromJust $ M.lookup ws myWorkspaceIndices

myManageHook :: XMonad.Query (Data.Monoid.Endo WindowSet)
myManageHook =
  composeAll . concat $
    [ [isDialog --> doCenterFloat]
    , [isFullscreen --> doFullFloat]
    , [className =? c --> doCenterFloat | c <- myCFloats]
    , [title =? t --> doFloat | t <- myTFloats]
    , [resource =? r --> doFloat | r <- myRFloats]
    , [resource =? i --> doIgnore | i <- myIgnores]
    , [(className =? x <||> title =? x <||> resource =? x) --> doShift (head myWorkspaces) | x <- my1Shifts]
    , [(className =? x <||> title =? x <||> resource =? x) --> doShift (myWorkspaces !! 1) | x <- my2Shifts]
    , [(className =? x <||> title =? x <||> resource =? x) --> doShift (myWorkspaces !! 2) | x <- my3Shifts]
    , [(className =? x <||> title =? x <||> resource =? x) --> doShift (myWorkspaces !! 3) | x <- my4Shifts]
    , [(className =? x <||> title =? x <||> resource =? x) --> doShift (myWorkspaces !! 4) | x <- my5Shifts]
    , [(className =? x <||> title =? x <||> resource =? x) --> doShift (myWorkspaces !! 5) | x <- my6Shifts]
    , [(className =? x <||> title =? x <||> resource =? x) --> doShift (myWorkspaces !! 6) | x <- my7Shifts]
    , [(className =? x <||> title =? x <||> resource =? x) --> doShift (myWorkspaces !! 7) | x <- my8Shifts]
    , [(className =? x <||> title =? x <||> resource =? x) --> doShift (myWorkspaces !! 8) | x <- my9Shifts]
    , [(className =? x <||> title =? x <||> resource =? x) --> doShift (myWorkspaces !! 9) | x <- my10Shifts]
    ]
  where
    -- using 'doShift ( myWorkspaces !! 7)' sends program to workspace 8!
    -- doShiftAndGo = doF . liftM2 (.) W.greedyView W.shift
    myCFloats = ["Arandr", "Galculator", "feh", "mpv", "Xfce4-terminal", "Yad", "confirm"]
    myTFloats = ["Downloads", "Save As...", "Bluetooth Devices", "Blanket"]
    myRFloats = []
    myIgnores = ["desktop_window"]
    my1Shifts = []
    my2Shifts = ["Firefox"]
    my3Shifts = ["Emacs", "Neovide"]
    my4Shifts = []
    my5Shifts = ["okular", "libreoffice"]
    my6Shifts = ["Gimp", "Inkscape"]
    my7Shifts = ["vlc", "mpv", "Wfica"]
    my8Shifts = ["Chromium"]
    my9Shifts = ["Virtualbox", "keepassxc"]
    my10Shifts = []
