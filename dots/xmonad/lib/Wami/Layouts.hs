{-# OPTIONS_GHC -Wno-missing-signatures #-}

module Wami.Layouts (myLayoutHook, myShowWNameTheme) where

import Wami.Constants (myBorderWidth, myFont)
import XMonad.Actions.MouseResize (mouseResize)
import XMonad.Hooks.ManageDocks (avoidStruts)
import XMonad.Hooks.ShowWName (SWNConfig (..))
import XMonad.Layout (Full (..), Mirror (..), (|||))
import XMonad.Layout.Accordion (Accordion (Accordion))
import XMonad.Layout.Grid (Grid (..))
import XMonad.Layout.LayoutModifier qualified
import XMonad.Layout.LimitWindows (limitWindows)
import XMonad.Layout.Magnifier (magnifier)
import XMonad.Layout.MultiToggle (
  EOT (EOT),
  mkToggle,
  single,
  (??),
 )
import XMonad.Layout.MultiToggle.Instances (
  StdTransformers (MIRROR, NBFULL, NOBORDERS),
 )
import XMonad.Layout.NoBorders (
  noBorders,
  smartBorders,
  withBorder,
 )
import XMonad.Layout.Renamed (Rename (Replace), renamed)
import XMonad.Layout.ResizableTile (ResizableTall (ResizableTall))
import XMonad.Layout.Simplest (Simplest (Simplest))
import XMonad.Layout.SimplestFloat (simplestFloat)
import XMonad.Layout.Spacing (
  Border (Border),
  Spacing,
  spacingRaw,
 )
import XMonad.Layout.Spiral (spiral)
import XMonad.Layout.SubLayouts (subLayout)
import XMonad.Layout.Tabbed (
  Theme (
    activeBorderColor,
    activeColor,
    activeTextColor,
    fontName,
    inactiveBorderColor,
    inactiveColor,
    inactiveTextColor
  ),
  addTabs,
  shrinkText,
  tabbed,
 )
import XMonad.Layout.ThreeColumns (ThreeCol (ThreeCol))
import XMonad.Layout.ToggleLayouts qualified as T
import XMonad.Layout.WindowArranger (windowArrange)
import XMonad.Layout.WindowNavigation (def, windowNavigation)

--
-- Makes setting the spacingRaw simpler to write. The spacingRaw module adds a configurable amount of space around windows.
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

-- Below is a variation of the above except no borders are applied
-- if fewer than two windows. So a single window has no gaps.
mySpacing' :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing' i = spacingRaw True (Border i i i i) True (Border i i i i) True

-- setting colors for tabs layout and tabs sublayout.
myTabTheme :: Theme
myTabTheme =
  def
    { fontName = myFont
    , activeColor = "#46d9ff"
    , inactiveColor = "#313846"
    , activeBorderColor = "#46d9ff"
    , inactiveBorderColor = "#282c34"
    , activeTextColor = "#282c34"
    , inactiveTextColor = "#d0d0d0"
    }

-- Theme for showWName which prints current workspace when you change workspaces.
myShowWNameTheme :: SWNConfig
myShowWNameTheme =
  def
    { swn_font = "xft:Iosevka:bold:size=40"
    , swn_fade = 1.0
    , swn_bgcolor = "#1c1f24"
    , swn_color = "#ffffff"
    }

-- The layout hook
myLayoutHook =
  avoidStruts $
    mouseResize $
      windowArrange $
        T.toggleLayouts monocle $
          mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
  where
    myDefaultLayout =
      withBorder myBorderWidth tall
        ||| threeCol
        ||| noBorders tabs
        ||| magnifi
        ||| floats
        ||| tallAccordion
        ||| wideAccordion
        ||| noBorders monocle
        ||| spirals
        ||| grid
        ||| threeRow

    -- Defining a bunch of layouts, many that I don't use.
    -- limitWindows n sets maximum number of windows displayed for layout.
    -- mySpacing n sets the gap size around the windows.
    magnifi =
      renamed [Replace "magnify"] $
        smartBorders $
          windowNavigation $
            addTabs shrinkText myTabTheme $
              subLayout [] (smartBorders Simplest) $
                magnifier $
                  limitWindows 12 $
                    mySpacing 4 $
                      ResizableTall 1 (3 / 100) (1 / 2) []

    tall =
      renamed [Replace "tall"] $
        smartBorders $
          windowNavigation $
            addTabs shrinkText myTabTheme $
              subLayout [] (smartBorders Simplest) $
                limitWindows 12 $
                  mySpacing 4 $
                    ResizableTall 1 (3 / 100) (1 / 2) []

    monocle =
      renamed [Replace "monocle"] $
        smartBorders $
          windowNavigation $
            addTabs shrinkText myTabTheme $
              subLayout [] (smartBorders Simplest) $
                limitWindows 20 Full

    floats =
      renamed [Replace "floats"] $
        smartBorders $
          limitWindows 20 simplestFloat

    grid =
      renamed [Replace "grid"] $
        smartBorders $
          windowNavigation $
            addTabs shrinkText myTabTheme $
              subLayout [] (smartBorders Simplest) $
                limitWindows 12 $
                  mySpacing 4 $
                    mkToggle (single MIRROR) $
                      Grid

    spirals =
      renamed [Replace "spirals"] $
        smartBorders $
          windowNavigation $
            addTabs shrinkText myTabTheme $
              subLayout [] (smartBorders Simplest) $
                mySpacing' 6 $
                  spiral (6 / 7)

    threeCol =
      renamed [Replace "threeCol"] $
        smartBorders $
          windowNavigation $
            addTabs shrinkText myTabTheme $
              subLayout [] (smartBorders Simplest) $
                limitWindows 7 $
                  ThreeCol 1 (3 / 100) (1 / 2)

    threeRow =
      renamed [Replace "threeRow"] $
        smartBorders $
          windowNavigation $
            addTabs shrinkText myTabTheme $
              subLayout [] (smartBorders Simplest) $
                limitWindows 7
                -- Mirror takes a layout and rotates it by 90 degrees.
                -- So we are applying Mirror to the ThreeCol layout.
                $
                  Mirror $
                    ThreeCol 1 (3 / 100) (1 / 2)

    tabs =
      renamed [Replace "tabs"]
      -- I cannot add spacing to this layout because it will
      -- add spacing between window and tabs which looks bad.
      $
        tabbed shrinkText myTabTheme

    tallAccordion = renamed [Replace "tallAccordion"] Accordion

    wideAccordion = renamed [Replace "wideAccordion"] $ Mirror Accordion
