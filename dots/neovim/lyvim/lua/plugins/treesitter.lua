return {
  "nvim-treesitter/nvim-treesitter",
  opts = function(_, opts)
    local more = {
      "bash",
      "dhall",
      "git_config",
      "gitignore",
      "haskell",
      "html",
      "javascript",
      "json",
      "lua",
      "markdown",
      "markdown_inline",
      "python",
      "regex",
      "rust",
      "scala",
      "sql",
      "terraform",
      "toml",
      "vim",
      "yaml",
    }
    vim.list_extend(opts.ensure_installed, more, 1, #more)
  end,
}
