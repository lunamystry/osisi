return {
  "folke/flash.nvim",
  enabled = false, -- not using it and it is a little distracting
  -- stylua: ignore
  keys = {
    { "s", mode = { "n", "x", "o" }, false, desc = "Flash" }, -- disabled because I use this a in macros (xi)
    { "S", mode = { "n", "x", "o" }, false, desc = "Flash Treesitter" }, -- disabled because I use this in macros (xi)
    { "r", mode = "o", function() require("flash").remote() end, desc = "Remote Flash" },
    { "R", mode = { "o", "x" }, function() require("flash").treesitter_search() end, desc = "Treesitter Search" },
    { "<c-s>", mode = { "c" }, function() require("flash").toggle() end, desc = "Toggle Flash Search" },
  },
}
